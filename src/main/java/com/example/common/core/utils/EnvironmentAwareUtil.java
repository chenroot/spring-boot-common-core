package com.example.common.core.utils;

import java.util.Properties;

public class EnvironmentAwareUtil {

    public static String adjust(){
        String tier = System.getenv("TIER");
        if(null == tier){
            //默认使用dev
            tier = "dev";
        }
        Properties props= System.getProperties();
        props.setProperty("spring.profiles.active", tier);
        props.setProperty("spring.config.location", "classpath:" + tier + "/");
        System.setProperties(props);
        return tier;
    }
}
